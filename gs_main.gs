

function testzzzzzzzzz(){
  
var v = 28700000;
var inf = v / 3600000;

//var inf = 7.99;
//var inf = 8.22;
var inf = 8.55;

Logger.log(inf);

var r = Math.floor(inf / 0.5) * 0.5;
//if (r < inf){
//  var pu = r + 0.5;
//}

Logger.log(r);
//Logger.log(pu);


}



/*
* 次Ｑ処理
*/
function updateQuota(){
  Logger.log("--updateQuota--");
  
  var app = SpreadsheetApp.getActiveSpreadsheet();
  var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("契約書");
  
  // 1.現Ｑ情報取得
  var nowQuarter = getNowQuarterValue();

  // 2.契約シートバックアップ(シート名：Ｑ情報)
  var copy_st = st.copyTo(app);
  copy_st.setName(nowQuarter.name);
//  copy_st.setName("検証中");

  // 3.Q更新前データ取得(退職者除く)
  var json = getContractViewInfoJSON2();
  var lst = json.filter(function($p){
    return $p.base.leavestate != "退職";
  });
  var arr = [];
  for(var i in lst){
    var r = lst[i];
    arr.push([
        r.manager.id + (r.managersub.id != "" ? ","+r.managersub.id : "") 
      , r.base.empno 
      , r.view_condition.workcompany 
      , r.view_condition.job 
      , r.view_condition.contracttype
      , r.base.empname_org 
      , r.base.empname_wrk 
      , r.base.empins 
      , r.base.socins 
      , r.base.socins_loss 
      , r.base.hiredate 
      , r.base.company 
      , "" 
      , "" 
      , r.view_condition.contractfixdate 
      , r.view_condition.salarytype 
      , r.view_condition.salarybase 
      , (r.old_condition.salarytype != r.view_condition.salarytype || r.old_condition.salarybase != r.view_condition.salarybase ? nowQuarter.name : r.base.last_salary_raise)
      , (
         r.old_condition.salarytype != r.view_condition.salarytype || r.old_condition.salarybase != r.view_condition.salarybase 
         ?  (r.old_condition.salarytype != r.view_condition.salarytype ? r.old_condition.salarytype+"→"+r.view_condition.salarytype+" " : "")
           +(r.old_condition.salarybase+"→"+r.view_condition.salarybase)
         : r.base.last_salary_raisememo
        )
      , r.view_condition.workingdays 
      , r.view_condition.workingtimes 
      , r.view_condition.workingtimesweek 
      , r.view_condition.paidvacationtimes 

      , r.view_condition.hourcondition1.from 
      , r.view_condition.hourcondition1.to  
      , r.view_condition.hourcondition1.week  
      , r.view_condition.hourcondition1.memo  

      , r.view_condition.hourcondition2.from 
      , r.view_condition.hourcondition2.to  
      , r.view_condition.hourcondition2.week  
      , r.view_condition.hourcondition2.memo  

      , r.view_condition.hourcondition3.from 
      , r.view_condition.hourcondition3.to  
      , r.view_condition.hourcondition3.week  
      , r.view_condition.hourcondition3.memo  

    ])
  };

  // 検証用コピー先で確認
//    // 4.シートクリア
//  var lastRow = copy_st.getLastRow();
//  var lastCol = copy_st.getLastColumn();
//  
//  var rg = copy_st.getRange(3, 1, lastRow-2, lastCol);
//  rg.clearContent();
//  
//  // 5.反映
//  var rg = copy_st.getRange(3, 1, arr.length, arr[0].length);
//  rg.setValues(arr);
  
  // 4.シートクリア
  var lastRow = st.getLastRow();
  var lastCol = st.getLastColumn();
  
  var rg = st.getRange(3, 1, lastRow-2, lastCol);
  rg.clearContent();
  
  // 5.反映
  var rg = st.getRange(3, 1, arr.length, arr[0].length);
  rg.setValues(arr);
  
  // 次Ｑ確定処理
  setNextQuota();
  
  return;
  
  
  
  
//  // シート内データ継承
//  var newrow = st.getLastRow();
//  var endrow = newrow - 2;
//  var startrow = 3;
//
//  
//  
//  
//  
//  // 【1】情報継承(コピー＆ペースト)
//  // コピー元
///*
//  var rgs = [
//                st.getRange(startrow, 36, endrow, 3)
//              , st.getRange(startrow, 39, endrow, 1)
//              , st.getRange(startrow, 41, endrow, 2)
//              , st.getRange(startrow, 44, endrow, 2)
//              , st.getRange(startrow, 47, endrow, 12)
//            ];
//  // ペースト先
//  var set_rgs= 
//            [
//                st.getRange(startrow, 3, endrow, 3)
//              , st.getRange(startrow, 15, endrow, 1)
//              , st.getRange(startrow, 16, endrow, 2)
//              , st.getRange(startrow, 20, endrow, 2)
//              , st.getRange(startrow, 23, endrow, 12)
//            ];
//*/
//  var rgs = [
//                st.getRange(startrow, 37, endrow, 3)
//              , st.getRange(startrow, 40, endrow, 1)
//              , st.getRange(startrow, 42, endrow, 2)
//              , st.getRange(startrow, 45, endrow, 4)
//              , st.getRange(startrow, 49, endrow, 12)
//            ];
//  // ペースト先
//  var set_rgs= 
//            [
//                st.getRange(startrow, 3, endrow, 3)
//              , st.getRange(startrow, 15, endrow, 1)
//              , st.getRange(startrow, 16, endrow, 2)
//              , st.getRange(startrow, 20, endrow, 4)
//              , st.getRange(startrow, 24, endrow, 12)
//            ];
//
//  for(var i = 0;i<rgs.length;i++){
//    rgs[i].copyTo(set_rgs[i]);
//  }
//
//  // 【2】値一括反映
//  /*
//  var base_rg1 = st.getRange(startrow, 65, endrow, 1);
//  var set_rg1  = st.getRange(startrow, 18, endrow, 2);
//  var base_value1 = base_rg1.getValues();
//  var original_value1 = set_rg1.getValues();
//  var set_values1 = [];
//  for(var i = 0;i<base_value1.length;i++){
//    if(base_value1[i][0] != ""){
//      // 金額変更がある場合、変更Q情報、昇給金額を設定
//      var tmp_arr = [nowQuarter.name, base_value1[i][0]];    
//      set_values1.push(tmp_arr);
//    }
//    else
//    {
//      // 金額変更が無い場合、元情報を設定
//      var tmp_arr = [original_value1[i][0], original_value1[i][1]];    
//      set_values1.push(tmp_arr);
//    }
//  }
//  set_rg1.setValues(set_values1);
//  */
//  var base_rg1 = st.getRange(startrow, 65, endrow, 1);
//  var set_rg1  = st.getRange(startrow, 18, endrow, 2);
//  var base_value1 = base_rg1.getValues();
//  var set_values1 = set_rg1.getValues();
//  for(var i = 0;i<base_value1.length;i++){
//    if(base_value1[i][0] != ""){
//      // 金額変更がある場合、変更Q情報、昇給金額を設定
//      set_values1[i][0] = nowQuarter.name;
//      set_values1[i][1] = "変更金額="+base_value1[i][0];
//    }
//  }
//  set_rg1.setValues(set_values1);
//  
//  // 【3】内容初期化(ブランク処理)
//  var clear_rg = [
//                st.getRange(startrow, 36, endrow, 29)
//              , st.getRange(startrow, 64, endrow, 4)
//            ];
//  for(var i = 0;i<clear_rg.length;i++){
//    clear_rg[i].clearContent();
//  } 
//
//  Logger.log("--setNextQuota--");
//
//  // 次Ｑ確定処理
//  setNextQuota();
  
}


/*
* Ｑ管理-次Ｑ処理
*/
function setNextQuota(){

  var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Q管理");
  var newrow = st.getLastRow();
  
  // 対象範囲取得
  var rg = st.getRange(2, 1, newrow - 1, 4)
  var values = rg.getValues();
   
  for(var i = 0;i<values.length;i++){
    if(values[i][3] != ""){
       // 現在行ブランク、次行有効化
       st.getRange(i + 2, 4).setValue("");
       st.getRange(i + 3, 4).setValue("〇");
       break;
     }
   }

}




/*
* ダブルクォート文字列除去
* 「"」で始まり「"」で終わる場合除去する
* "xxx"→ xxx
* "xxx →"xxx
*  xxx"→ xxx"
*  xxx → xxx
*/
function remove_dquart(v){
 
  if(v.match(/^".+?"(\n|\r)$/)){
    return v.substring(1,v.length - 2);
  }
  else
  if( v.match(/^".+?"$/)){
    return v.substring(1,v.length - 1);
  }
  else
  {
    return v; 
  }
  
//  return v.match(/^".+?"(\n|\r)*$/) == null ? v : v.substring(1,v.length - 1);
}


function getWorktimeinfoAcquireFromMemo($memo){

  var result = {"workingday":"", "workingtimes":""};
  
  // 週労働日数
  var tmp = $memo.match(/週\d日/);
  if(tmp){
    result.workingday = tmp[0].substr(1,1);
  }
  
  // 労働時間
  // 1.[5～7h] → [5～7]
  if($memo.match(/\d(~|～|-)\dh/)){
    var tmp = $memo.match(/\d(~|～|-)\dh/);
    result.workingtimes = tmp[0].substr(0,1)+"～"+tmp[0].substr(2,1);
  }
  else
  // 2.[7.5h] → [7～8]
  if($memo.match(/\d\.\dh/)){
    var tmp = $memo.match(/\d\.\dh/);
    var num = Number(tmp[0].substr(0,1));
    result.workingtimes = num+"～"+(num+1);
  }
  else
  // 3.[7h] → [7]
  if($memo.match(/\dh/)){
    var tmp = $memo.match(/\dh/);
    result.workingtimes = tmp[0].substr(0,1);
  }
  
  return result;
}

function getDisplayWorkingtimesweek($wd, $wt){
  var arr1 = $wd.split(/~|～/);
  var arr2 = $wt.split(/~|～/);
  var cal = [];
  if(arr1.length == 1 && arr2.length == 1){
    cal.push(arr1[0] * arr2[0]);
  }
  else
  {
    if(arr1.length == 1){
      arr1.push(arr1[0]);
    }
    if(arr2.length == 1){
      arr2.push(arr2[0]);
    }
    cal.push(arr1[0] * arr2[0]);
    cal.push(arr1[1] * arr2[1]);
  } 
  var weektime = cal.length == 1 ? cal[0] : cal[0] + "～" + cal[1];

  return weektime;
}

/*
* CSV→契約書情報反映処理
*/
function writeSheet(csvarray) {

  try{

    // 1.CSVを2次元配列に格納
    Logger.log("01.CSVを2次元配列に格納");
    var lineArr = csvarray.split("\n");
    var itemArr = [];
    for (var i = 0; i < lineArr.length; i++) {
      itemArr[i] = lineArr[i].split(",");
    }

    // 2.レイアウトチェック
    Logger.log("02.レイアウトチェック");
    if(!(remove_dquart(itemArr[0][0]) == "社員番号" && remove_dquart(itemArr[0][1]) == "雇用区分コード")){
      throw 1;
    }

    // 3.シートの契約情報より登録済み社員番号を取得
    Logger.log("03.シートの契約情報より登録済み社員番号を取得");
    // 契約情報取得
    var json = getContractViewInfoJSON2();    
    var empnos = [];
    var fncSearchEmp = function($no){
      var fil = json.filter(function(p){
        return p.base.empno == $no;
      });
      return fil.length == 0 ? null : fil[0];
    };
//    
//Logger.log("--------------------");
//var tmp1 = fncSearchEmp("000345");
//var tmp2 = fncSearchEmp("00034X");
//    Logger.log("tmp1="+tmp1.base.empno+":"+tmp1.rowid);
//Logger.log("tmp2="+tmp2);
        
    // 4.各種付与情報準備
    Logger.log("04.各種付与情報準備");
    // 社員情報検索関数
    var empSearchF = getSearchEmpFunction();
    // 有休付与日数
    var paidholidaysDic = [];
    paidholidaysDic["01"] = "週5";
    paidholidaysDic["02"] = "週4（30時間未満）";
    paidholidaysDic["03"] = "週3";
    paidholidaysDic["04"] = "週2";
    paidholidaysDic["05"] = "週1";
    paidholidaysDic["06"] = "週4（30時間以上）";    

    // 登録シート
    var appendRowInfo = [];
    var updateRowInfo = [];
  
    // 5.反映処理
    Logger.log("05.反映処理:itemArr.length="+itemArr.length);
    for(var i = 1;i<itemArr.length;i++){
      // 社員番号なし（ブランク行）の場合、処理しない
Logger.log("itemArr[i].length="+itemArr[i].length);
      if(itemArr[i].length <= 1) continue;
      
      // 5.1.登録済みデータ確認
      var no = remove_dquart(itemArr[i][0]);
      var isEmpInf = fncSearchEmp(no);  
      // 5.2.未登録時処理
      // --------------------------------------------------
      // 直属上司メールアドレス取得（初期管理者設定)
      var manager = remove_dquart(itemArr[i][16]).match(/^.+:/);
      var manager_mailaddress = "";
      if(manager){
        // 上司社員番号
        var empno   = manager[0].substr(0,6);
        var empinfo = empSearchF(empno);
        if(empinfo){
          manager_mailaddress = empinfo.mail;
        }
      }
          
      // 労働時間情報取得
      var wt = getWorktimeinfoAcquireFromMemo(remove_dquart(itemArr[i][13]));    
      // 有給時間
      var paidholidays = remove_dquart(itemArr[i][14]) + ":" + remove_dquart(itemArr[i][15]);
      if(paidholidaysDic[remove_dquart(itemArr[i][14])]){
        paidholidays = remove_dquart(itemArr[i][14]) + ":" + paidholidaysDic[remove_dquart(itemArr[i][14])];
      }

      // 取込ユーザ情報
      var userEmpNo = remove_dquart(itemArr[i][0]);
      var userInfo = empSearchF(userEmpNo);

      if(!isEmpInf){
Logger.log("--①－１--");

        // ①追加対象
        var rowInfo = [
            manager_mailaddress          // 管理者
          , userEmpNo                    // 社員番号
          , ""                           // 就業場所
          , ""                           // 仕事内容
          , remove_dquart(itemArr[i][2]) // 雇用区分
          , remove_dquart(itemArr[i][3]) // 氏名
          , remove_dquart(itemArr[i][4]) // 職場氏名
          , remove_dquart(itemArr[i][5]) + ":" + remove_dquart(itemArr[i][6]) // 雇用保険区分
          , remove_dquart(itemArr[i][7]) + ":" + remove_dquart(itemArr[i][8]) // 社保加入区分
          , remove_dquart(itemArr[i][18]) // 健康保険資格喪失年月日
          , remove_dquart(itemArr[i][9]) // 入社日
          , (userInfo ? userInfo.company : "") // 所属会社
          , "" // 最終昇給日(ダミー1)
          , "" // 備考(ダミー2)
          , "" // 契約社員のみ現在の契約期間満了日
          , remove_dquart(itemArr[i][11]) // 給与区分
          , remove_dquart(itemArr[i][12]) // 基本給
          , "" // 最終昇給日
          , "" // 備考
          , wt.workingday   // 所定労働日数
          , wt.workingtimes // 労働時間
          , getDisplayWorkingtimesweek(wt.workingday, wt.workingtimes) // 労働時間／週
          , paidholidays // 有給時間
          ];
        // データ追加
        appendRowInfo.push(rowInfo);  
Logger.log("--①－２--");
      }
      else
      {
Logger.log("--②－１--");
        // ②更新対象
        Logger.log(isEmpInf.managersub.id+":"+(isEmpInf.managersub.id==""));
        var manager_mailaddress = isEmpInf.manager.id + (isEmpInf.managersub.id == "" ?  "" : ","+isEmpInf.managersub.id);
        var rowInfo = [
            manager_mailaddress                 // 管理者
          , userEmpNo                           // 社員番号
          , isEmpInf.old_condition.workcompany  // 就業場所
          , isEmpInf.old_condition.job          // 仕事内容
          , remove_dquart(itemArr[i][2])        // 雇用区分
          , remove_dquart(itemArr[i][3])        // 氏名
          , remove_dquart(itemArr[i][4])        // 職場氏名
          , remove_dquart(itemArr[i][5]) + ":" + remove_dquart(itemArr[i][6]) // 雇用保険区分
          , remove_dquart(itemArr[i][7]) + ":" + remove_dquart(itemArr[i][8]) // 社保加入区分
          , remove_dquart(itemArr[i][18]) // 健康保険資格喪失年月日
          , remove_dquart(itemArr[i][9]) // 入社日
          , (userInfo ? userInfo.company : "") // 所属会社
          , "☆☆☆" // 最終昇給日(ダミー1)
          , "☆☆☆" // 備考(ダミー2)
          , isEmpInf.old_condition.contractfixdate // 契約社員のみ現在の契約期間満了日
          , remove_dquart(itemArr[i][11]) // 給与区分
          , remove_dquart(itemArr[i][12]) // 基本給
//          , "" // 最終昇給日
//          , "" // 備考
//          , wt.workingday   // 所定労働日数
//          , wt.workingtimes // 労働時間
//          , getDisplayWorkingtimesweek(wt.workingday, wt.workingtimes) // 労働時間／週
//          , paidholidays // 有給時間
          ];
        
Logger.log("--rowInfo--");
Logger.log(rowInfo);
        
        
        var param = {"rowid":isEmpInf.rowid,"rowInfo":rowInfo};
        updateRowInfo.push(param);
      }
      
      
    }

    // 対象有の場合追加
    Logger.log("appendRowInfo.length="+appendRowInfo.length);
    if(appendRowInfo.length > 0){
      var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("契約書"); 
      var row = st.getLastRow() + 1;
      var rg = st.getRange(row, 1, appendRowInfo.length, 23);
      rg.setValues(appendRowInfo);
    }
    // 更新有の場合
    if(updateRowInfo.length > 0){
      var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("契約書"); 
      for(var i in updateRowInfo){
        var p = updateRowInfo[i];
        
        Logger.log("rowid="+p.rowid);
        Logger.log("p.rowInfo="+p.rowInfo);
        
        var rg = st.getRange(p.rowid, 1, 1, 17);
        var arr = [p.rowInfo];
        rg.setValues(arr);
      }

    }    
    
  }catch(e){
    switch(e){
      case 1:
        throw "アップロードファイルのレイアウトが異なります。取込用CSVを選択してください。";
        break;
      default:
        throw "想定外エラー：" + e;
        break;
    }
  }

}


/*
* ページ表示時処理
*/
function doGet(e){

Logger.log(1);
  // [従業員情報]
  var allEmployee = getJsonMst("従業員");
  allEmployee = JSON.stringify(allEmployee);
Logger.log(2);
  // [ユーザ権限取得]
  var auth = getLoginUserAuth();
Logger.log(3);
  var user_auth = JSON.stringify(auth);
  // [システム状態取得]
Logger.log(4);
  var dic = getSettingInfoDictionary();
Logger.log(5);
  // [現在Q情報]
  var nowQuarter = getNowQuarterValue();
Logger.log(6);
  
  // [テンプレート]
  var t = HtmlService.createTemplateFromFile("html_base");
  t.systemStatus = dic["システムロック"];
  t.auth         = auth;
  t.user_auth    = user_auth;
  t.nowQuarter     = nowQuarter;
  t.allEmployee  = allEmployee;
  
  return t.evaluate().setTitle('契約書情報収集管理');
  
  
  
  
}


/*
* ログインユーザメールアドレス取得
*/
function getLoginUserMailAddress(){
  var u = Session.getActiveUser();
  return u.getEmail();
}

/*
* 現在Ｑ情報取得
*/
function getNowQuarterValue(){

  var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Q管理");
  var newrow = st.getLastRow();
  
  // 対象範囲取得
  var rg = st.getRange(2, 1, newrow - 1, 4)
  var values = rg.getValues();
 
   var ret;
   
   for(var i = 0;i<values.length;i++){
     if(values[i][3] != ""){
      // 設定
      ret = {
                          "name"          : values[i][0]
                        , "from"          : Moment.moment(values[i][1]).format("YYYY/MM/DD")
                        , "to"            : Moment.moment(values[i][2]).format("YYYY/MM/DD")
            };
       break;
     }
   }

  // 契約満了日
  var tmp = [
    Moment.moment(ret.to).add("M", 1).format("YYYY/MM/01"),
    Moment.moment(ret.to).add("M", 4).format("YYYY/MM/01"),
    Moment.moment(ret.to).add("M", 7).format("YYYY/MM/01"),
    Moment.moment(ret.to).add("M", 10).format("YYYY/MM/01"),
  ];
  ret.contractExpirationDates = tmp.map(function($p){
    return Moment.moment($p).add("d", -1).format("YYYY/MM/DD");
  });

  return ret;
}

//function test_________________________x(){
//  
//  var x = "2018/06/30";
//  
//  var base = Moment.moment(x);
//  
//
//  var aga = [
//    Moment.moment(x).add("M", 4).format("YYYY/MM/01"),
//    Moment.moment(x).add("M", 7).format("YYYY/MM/01"),
//    Moment.moment(x).add("M", 10).format("YYYY/MM/01"),
//    Moment.moment(x).add("M", 13).format("YYYY/MM/01"),
//  ];
//  
//  var rep = aga.map(function($p){
//    return Moment.moment($p).add("d", -1).format("YYYY/MM/DD");
//  });
//
//  Logger.log(aga);
//  Logger.log(rep);
//
//}





/*
* 表示用HTML取得
*/
function view_page(idx, key){
  
  // ユーザ権限
  var auth = getLoginUserAuth();
  
  switch(idx){
//    case 1:
//      // 従業員一覧
//      // 1.一覧表示
//      var t = HtmlService.createTemplateFromFile("html_001_emplist").evaluate();
//      return t.getContent();
//      break;
    case 2:
      // 従業員管理者変更一覧
      // 1.テンプレート取得
      var t = HtmlService.createTemplateFromFile("html_002_changemanager");
      // 2.データ取得
      // 契約従業員情報
      var values = getContractViewInfoJSON(auth.mail, auth.change_manager != "");
      t.values = values;
      // 2.退職者情報
      var arr = [];
      for(var i in values){
        var p = values[i];
        if(p.manager.id == ""){arr.push(p.base.empname);};
           
        
        
//Logger.log("id="+p.manager.id);       
//        if(p.manager.id != "" && p.manager.name == ""){
//Logger.log("p.manager.id="+p.manager.id);       
//          if(arr.indexOf(p.manager.id) == -1){ arr.push(p.manager.id);};
//        }
//        if(p.managersub.id != "" && p.managersub.name == ""){
//Logger.log("p.managersub.id="+p.managersub.id);       
//          if(arr.indexOf(p.managersub.id) == -1){ arr.push(p.managersub.id);};
//        }
      }       
      t.noneManager = arr;
           
      return t.evaluate().getContent();
      break;
    case 3:
      // ユーザ権限管理
      // 1.テンプレート取得
      var t = HtmlService.createTemplateFromFile("html_003_manageuser");
      var values = getManageUsers(false);
      t.values = values;
      return t.evaluate().getContent()
      break;
    case 4:
      // 【管理者】システム管理
      // 1.テンプレート取得
      var t = HtmlService.createTemplateFromFile("html_004_status_check");
      // 2.データ取得
      // マスタ情報
      t.settinginfo = getSettingInfoParameter();
      // 3.戻り値設定      
      return t.evaluate().getContent();
      break;
    case 6:
      // 対応状況一覧
      // 1.テンプレート取得
      var t = HtmlService.createTemplateFromFile("html_006_remind_mail");
      // 2.データ取得
      // 契約従業員情報
      var values  = getContractViewInfoJSON(auth.mail, auth.remind_mail != "");     
      var fvalues = values.filter(function(p){
        return p.proctype == "" || p.proctype == "調整中（雇用形態変更）" || p.proctype == "調整中（その他）";
      });
      t.values = fvalues;
      // 処理状況
      t.statusCount = getContractCountView(values);
      // マスタ情報
      t.settinginfo = getSettingInfoParameter();
      // 3.戻り値設定      
      return t.evaluate().getContent();
      break;
    case 101:
    Logger.log("a0");
      // 従業員一覧(静的)
      // 1.テンプレート取得
      var t = HtmlService.createTemplateFromFile("html_001_emplist");
    Logger.log("a1");
      // 2.データ取得
      // 契約従業員情報
      var values = getContractViewInfoJSON(auth.mail, auth.view_contract_list != "");
    Logger.log("values="+values);
      t.values = values;
      // 選択リスト内容(マスタ)
      var options = getHtmlOptionList();
      t.opt1 = options.WorkPlace;
      t.opt2 = options.JobDep;
      t.opt3 = options.Jobs;
      t.opt4 = options.ContractExpirationDates;
    Logger.log("a2");
      
      // 3.戻り値設定
      var ret ={"html":t.evaluate().getContent(),"values":values};
      
      
      //Logger.log(ret.html);
    Logger.log("a3");
      return JSON.stringify(ret);
      break;
  }

}



/*
* 新旧値を比較して、新値に設定されている場合はその値を適用する
* value : 取得元値
* hm    : h=時間要素を取得、m=分要素を取得
*/
function getCP(oldvalue, newvalue){
  return newvalue != "" ? newvalue : oldvalue;
}

function setContractInfoJSON(p){
  // 情報設定
  // ※対象シートは後程動的処理
  var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("契約書");
  
  var rg = st.getRange(p.rowid, 36, 1, 30);
  var values = [];
  values.push(
                [
                   p.proctype
                 , p.set_condition.workcompany
                 , p.set_condition.job
                 , p.set_condition.contracttype
                 , p.set_condition.contractfixdate
                 , p.set_condition.retiredate
                 , p.set_condition.salarytype
                 , p.set_condition.salarybase
                 , p.set_condition.payrisememo
                 , p.set_condition.workingdays
                 , p.set_condition.workingtimes
                 , p.set_condition.workingtimesweek
                 , p.set_condition.paidvacationtimes
                 , p.set_condition.hourcondition1.from
                 , p.set_condition.hourcondition1.to
                 , p.set_condition.hourcondition1.week
                 , p.set_condition.hourcondition1.memo
                 , p.set_condition.hourcondition2.from
                 , p.set_condition.hourcondition2.to
                 , p.set_condition.hourcondition2.week
                 , p.set_condition.hourcondition2.memo
                 , p.set_condition.hourcondition3.from
                 , p.set_condition.hourcondition3.to
                 , p.set_condition.hourcondition3.week
                 , p.set_condition.hourcondition3.memo
                 , p.set_condition.laborinterview
                 , p.set_condition.laborinterviewmemo
                 , p.set_condition.changed_salarytype
                 , p.set_condition.changed_salary
                 , p.set_condition.changed_salarydiff
               ]
  );
  rg.setValues(values);
}


/*
* 従業員管理者情報反映
*/
function setEmpManagerInfoJSON(prm){
  Logger.log(prm);
  Logger.log(prm[0].rowid);

  // 情報設定
  // ※対象シートは後程動的処理
  var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("契約書");
  
  for(var i = 0;i<prm.length;i++){
    var p = prm[i];
    var rg = st.getRange(p.rowid, 1, 1, 1);
    var values = [];
    values[0] =[
                   p.manageuser
               ];

    rg.setValues(values);
    
  }
}

function setManageuserInfoJSON(prm){

  var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("ユーザ管理");
  
  var changeIds = [];
  var changecount = 0;
  var values = [];
  for(var i = 0;i<prm.length;i++){
    var p = prm[i];
    var row =[
                   p.mail
                 , p.name
                 , p.belong
                 , p.view_contract_list
                 , p.control_usermenu
                 , p.change_manager
                 , p.manage_system
                 , p.remind_mail
               ];
    values.push(row);
  }
  
  // 対象シートクリア
  var deleteTargetRange = st.getRange(2, 1, st.getLastRow(), 8);
  deleteTargetRange.clear();
  
  // 値一括設定
  var rg = st.getRange(2, 1, values.length, 8);
  rg.setValues(values);

}

// 指定シートの最終行取得
function getLastRow(sheetname){
  var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheetname);
  var lastrow = stEmp.getLastRow();  
  return lastrow;
}

function setSettingInfo($kv){
  // 情報取得
  var values = getSpreadsheetValues("設定", 2, 1, 2);
  // 対象判定
  for(var idx in $kv){
    for(var i = 0;i<values.length;i++){
      if(values[i][0] == $kv[idx].key){
        values[i][1] = $kv[idx].value;
        break;
      }
    }
  }
  // 保存
  saveSpreadsheetValues("設定", 2, 1, values);
}


function getSettingInfoDictionary(){
  
  var lst = getSpreadsheetValues("設定", 2, 1, 2);
  var dic = [];
  for(var i = 0;i<lst.length;i++){
    dic[lst[i][0]] = lst[i][1];
  }
  
  return dic;
}

function getSettingInfoParameter(){
  
  var dic = getSettingInfoDictionary();
  var settinginfo = {
      mail1    : dic["催促メール雛形"]
    , mail2    : dic["対応周知メール雛形"]
    , mst1     : dic["勤務地リスト"]
    , mst2     : dic["作業内容部門リスト"]
    , mst3     : dic["作業内容業種リスト"]
    , syslock  : dic["システムロック"]
    , mail_cc  : dic["メール送信追加CC"]
    , mail_bcc : dic["メール送信追加BCC"]
  };
  return settinginfo;
}


function getHtmlOptionList(){
 
  // 設定情報取得
  var dic = getSettingInfoDictionary();
  
  // 処理用パラメータ
  var prm = [{name:"WorkPlace",key:"勤務地リスト"},{name:"JobDep",key:"作業内容部門リスト"},{name:"Jobs",key:"作業内容業種リスト"},];
  var result = {"WorkPlace":"","JobDep":"", "Jobs":""};
             
  for(var idx in prm){
    var value = dic[prm[idx].key];
    var arr = value.split("\n");
    var html = "<option value=''>--選択してください--</option>";
    for(var i =0;i<arr.length;i++){
      html += "<option value='"+arr[i]+"'>"+arr[i]+"</option>"
    }
    // 設定
    result[prm[idx].name] = html;
  }
  
  // Q毎の対象契約満了日追加
  // =====================================
  var qv = getNowQuarterValue();
  var html = "<option value=''>設定なし</option>";
  for(var i in qv.contractExpirationDates){
    var p = qv.contractExpirationDates[i];
    html += "<option value='"+p+"'>"+p+"</option>"
  }
  result["ContractExpirationDates"] = html;
  // =====================================

  return result;
}

/*
  ログインユーザの権限取得
*/
function getLoginUserAuth(){
  // ユーザ権限一覧取得(連想配列)
  var userAuthArray = getManageUsers(true);
  // ログインユーザID取得
  var loginUserId   = getLoginUserMailAddress();
  
  var auth = {
      mail: loginUserId
    , name: ""
    , belonging:""   
    , view_contract_list  : ""
    , control_usermenu    : ""
    , change_manager      : ""
    , manage_system       : ""
  }
  if(userAuthArray[loginUserId]){
    auth = userAuthArray[loginUserId];
  }
  
  return auth;

}

/*
  システム状態(ロック、アンロック)設定
*/
function updateSystemLockStatus($lock){ 
  var lst = [];
  lst.push({"key":"システムロック","value":$lock})
  setSettingInfo(lst);
}

/**
* 
* スプレッドシード情報取得処理
* @param  {text} sheetName  sheetName 対象シート名
* @param  {int}  startRow   開始行
* @param  {int}  startCol   開始列
* @param  {int}  colcount   対象列数
* @return {object[][]}　    取得配列
*/
function getSpreadsheetValues(sheetName, startRow, startCol, colcount){
  // シート
  var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheetName);
  // 最終行
  var endrow = st.getLastRow();
  // 対象行数
  var rowcount = endrow - (startRow - 1);
  // 対象範囲取得
  var rg = st.getRange(startRow, startCol, rowcount, colcount)
  // 配列取得
  var values = rg.getValues();
  
  return values;
}

/**
* 
* スプレッドシート情報設定
* @param {text}     sheetName sheetName 対象シート名
* @param {int}      startRow  開始行
* @param {int}      startCol  開始列
* @param {object[]} values    設定配列情報
*/
function saveSpreadsheetValues(sheetName, startRow, startCol, values){
  // 対象外
  if(!values || values.length == 0 || values[0].length == 0){return;};
  
  // シート
  var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheetName);
  // 対象範囲取得
  var rg = st.getRange(startRow, startCol, values.length, values[0].length)
  // 値更新
  rg.setValues(values);

}



/*
* システム登録ユーザ一覧
* mode:true=メールアドレス連想配列、false=配列
*/
function getManageUsers(mode){
  
  var values = getSpreadsheetValues("ユーザ管理", 2, 1, 8);
  
  // 最新ユーザ情報より
  var empSearch = getSearchEmpFunction();
  
  var dic = [];
  dic["1"] = "労務管理者";
  dic["2"] = "管理者";
  dic["3"] = "一般";
     
  var arr=[];
  var retarr = [];
  for(var i = 0;i<values.length;i++){
    var emp = empSearch(values[i][0]);
    
    //Logger.log(values[i][0]);
    var tmp = {
                  mail: values[i][0]
                , name: emp ? emp.name : ""
                , belonging:emp ? emp.affiliation_name : ""
                , to:values[i][3]
                , cc:values[i][4]
                , bcc:values[i][5]
                , type:values[i][6]
                , typename:dic[values[i][6]]
      
                , view_contract_list  : values[i][3] != "" ? "〇" : ""
                , control_usermenu    : values[i][4] != "" ? "〇" : ""
                , change_manager      : values[i][5] != "" ? "〇" : ""
                , manage_system       : values[i][6] != "" ? "〇" : ""
                , remind_mail         : values[i][7] != "" ? "〇" : ""
                , rowid:(i + 2)
              }
    if(mode){
      // 連想配列
      arr[values[i][1]] = tmp;
    }
    else
    {
      // 配列
      retarr.push(tmp);
    }
  }
  
  if(mode){
    // 氏名でソート後、再設定
    var forsort = Object.keys(arr);
    forsort.sort();
    for(var i = 0; i < forsort.length; i++ ) {
      retarr[arr[forsort[i]].mail] = arr[forsort[i]];
    }
  }
  else
  {
    // 権限:降順、所属部門:昇順、氏名：昇順
    retarr = retarr.sort(function(a,b){
//      // 1.権限:降順
//      if(a.type < b.type){return -1;}
//      if(a.type > b.type){return 1;}
      // 2.所属部門:昇順
      if(a.belonging > b.belonging){return -1;}
      if(a.belonging < b.belonging){return 1;}
      // 3.氏名：昇順
      if(a.name > b.name){return -1;}
      if(a.name < b.name){return 1;}
    });
      
  }

  return retarr;
}

/*
* 事業部マスタ取得
*/
function getDepartment(mode){

  var lst = getJsonMst("組織図");
  
  var filter = lst.filter(function(p){
    return p.hierarchy == 1;
  });
  
  var arr=[];
  for(var i in filter){
//    var tmp = {
//                  id: filter[i].key
//                , name: filter[i].name
//              }
    var tmp = {
                  id: filter[i].name
                , name: filter[i].name
              }
    arr[tmp.id] = tmp;
  }
  
//  // 情報設定
//  var st = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("事業部");
//  
//  // 最終行
//  var newrow = st.getLastRow();
//  
//  // 対象範囲取得
//  var rg = st.getRange(2, 1, newrow - 1, 2)
//  var values = rg.getValues();
//
//  var arr=[];
//  for(var i = 0;i<values.length;i++){
//    Logger.log(values[i][0]);
//    var tmp = {
//                  id: values[i][0]
//                , name: values[i][1]
//              }
//    arr[values[i][0]] = tmp;
//  }
  
  // 氏名でソート後、再設定
  var keys = Object.keys(arr);
  var retarr = [];
  for(var i = 0; i < keys.length; i++ ) {
    if(mode){
      retarr[arr[keys[i]].id] = arr[keys[i]];
    }
    else
    {
      retarr.push(arr[keys[i]]);
    }
  }
//  if(mode){
//    Logger.log(arr["HC000000"]);
//    Logger.log(arr["HR000000"]);
//  }
  
Logger.log("--[End]getDepartment--");  

  return retarr;
}



function getContractCountView(values){
 
  var json = {
      "none":0
    , "exist":0
    , "other":0
    , "adjusting_changecontract":0
    , "adjusting_other":0
    , "def":0
  };
  
  // 未対応、調整中対象
  var untreatedTarget = [];
  for(var i=0;i < values.length;i++){
    var p = values[i];
    switch(p.proctype){
      case "無し":
        json.none++;
        break;
      case "有り":
        json.exist++;
        break;
      case "更新対象外":
        json.other++;
        break;
      case "調整中（雇用形態変更）":
        json.adjusting_changecontract++;
        break;
      case "調整中（その他）":
        json.adjusting_other++;
        break;
      default:
        json.def++;
        break;
    }
  }
  return json;
}

function getCsv($p){
  
  var buf = "";
  switch($p.type){
    case "ChangeSalary":
      buf = getCsvChangeSalary();
      break;
    case "SettingInterview":
      buf = getCsvSettingInterview();
      break;
    case "PayrollAccounting":
      buf = getCsvPayrollAccounting();
      break;
    case "UpdateBugyo":
      buf = getCsvUpdateBugyo($p.company);
      break;
    case "Mailaddress":
      buf = getCsvMailaddress();
      break;
  }
    
  return buf;
}


function getCsvChangeSalary(){
  
  var json = getContractViewInfoJSON2();
  var lst = json.filter(function($p){
    return $p.proctype != "無し"
           &&
           $p.proctype != "更新対象外"
           &&
           (
                $p.view_condition.salarytype != $p.old_condition.salarytype
             || $p.view_condition.salarybase != $p.old_condition.salarybase
           );
  });  
  
//  var csv = [["社員番号","氏名","契約形態","変更前-給与区分","変更前-基本給","変更後-給与区分","変更後-基本給"]];
  var csv = [["社員番号","氏名","所属部署","改定前給与区分","改定前給与額","改定後給与区分","改定後給与額","昇給推薦理由","昇給額","週の労働時間","昇給額（月額）"]];
  //									
  
  var fncCalcSalary = function($old_workingtimesweek, $old_salarytype, $old_salarybase, $new_workingtimesweek, $new_salarytype, $new_salarybase){
    
//    Logger.log("$new_workingtimesweek="+$new_workingtimesweek);
//    Logger.log("$old_salarytype="+$old_salarytype);
//    Logger.log("$old_salarybase="+$old_salarybase);
//    Logger.log("$new_salarytype="+$new_salarytype);
//    Logger.log("$new_salarybase="+$new_salarybase);
    
    
    var result = $new_salarybase;
    if($old_salarytype =="時給" && $new_salarytype == "時給"){
      var arr = $new_workingtimesweek.split(/~|～/);
      var diff = $new_salarybase - $old_salarybase;
      if(arr.length == 1){
        result = Number(arr[0]) * Number(diff) * 4;
      }
      else
      {
//        result = (Number(arr[0]) * Number(diff) * 4) + "～" + (Number(arr[1]) * Number(diff) * 4);        
        result = (Number(arr[1]) * Number(diff) * 4);        
      }
    }
    else
    if($old_salarytype =="月給" && $new_salarytype == "月給"){
      result = Number($new_salarybase) - Number($old_salarybase);
    }
    else
    if($old_salarytype =="月給" && $new_salarytype == "時給"){
      var arr = $new_workingtimesweek.split(/~|～/);
      if(arr.length = 1){
        result = (Number(arr[0]) * Number($new_salarybase) * 4);
      }
      else
      {
//        result = (Number(arr[0]) * Number($new_salarybase) * 4) + "～" + (Number(arr[1]) * Number($new_salarybase) * 4);        
        result = (Number(arr[1]) * Number($new_salarybase) * 4);        
      }
      
//      var arr = $new_workingtimesweek.split(/~|～/);
//      if(arr.length = 1){
//        result = (Number(arr[0]) * Number($new_salarybase) * 4) - Number($old_salarybase);
//      }
//      else
//      {
//        result = ((Number(arr[0]) * Number($new_salarybase) * 4) - Number($old_salarybase)) + "～" + ((Number(arr[1]) * Number($new_salarybase) * 4) - Number($old_salarybase));        
//      }
    }
    else
    if($old_salarytype =="時給" && $new_salarytype == "月給"){
      result = $new_salarybase;
//      var arr = $old_workingtimesweek.split(/~|～/);
//      if(arr.length = 1){
//        result = Number($new_salarytype) - (Number(arr[0]) * Number($old_salarybase) * 4);
//      }
//      else
//      {
//        result = (Number($new_salarytype) - (Number(arr[0]) * Number($old_salarybase) * 4)) + "～" + (Number($new_salarytype) - (Number(arr[1]) * Number($old_salarybase) * 4));        
//      }

    }

    return result;
  }
  
  for(var i in lst){
    var r = lst[i];
    
    // 月額給与(時給時は計算)
    var newSalary = fncCalcSalary(  r.old_condition.workingtimesweek
                                  , r.old_condition.salarytype
                                  , r.old_condition.salarybase
                                  , r.view_condition.workingtimesweek
                                  , r.view_condition.salarytype
                                  , r.view_condition.salarybase
    );
        
    csv.push(
             [
                 '"'+r.base.empno+'"'
               , '"'+r.base.empname+'"'
               , '"'+r.base.affiliation_name+'"'
               , '"'+r.old_condition.salarytype+'"'
               , '"'+r.old_condition.salarybase+'"'
               , '"'+r.view_condition.salarytype+'"'
               , '"'+r.view_condition.salarybase+'"'
               , '"'+r.view_condition.payrisememo+'"' //"昇給推薦理由"
               , '"'+(r.old_condition.salarytype == r.view_condition.salarytype ? r.view_condition.salarybase - r.old_condition.salarybase : "区分変更")+'"'
               , '"'+r.view_condition.workingtimesweek+'"'
               , '"'+newSalary+'"'
             ]
            );
//    Logger.log(r.base.empname+":"+r.view_condition.salarytype+":"+r.view_condition.salarybase);
  }

  return csv.map(function(p){return p.join(",")}).join("\r\n");
}

// 面談設定用CSV
function getCsvSettingInterview(){
  
  var json = getContractViewInfoJSON2();
  
//  for(var i in json){
//    var p = json[i];
//    Logger.log(p.view_condition.retiredate + ":p.proctype=" + p.proctype + ":p.view_condition.retiredate=" + p.view_condition.retiredate + ":" + (p.view_condition.retiredate != "" && p.proctype == "有り"));
//  }
    
  var lst = json.filter(function($p){
    return (
             ($p.view_condition.retiredate != "" && $p.proctype == "有り") 
             ||
             ($p.proctype == "無し" || $p.proctype == "調整中（雇用形態変更）" || $p.proctype == "調整中（その他）")
           )
           ;
  });  
  
  var csv = [["社員番号","氏名","就業場所（奉行）","就業場所（ツール）","管理者名","更新有無","退職日"]];
Logger.log("lst.length="+lst.length);
  for(var i in lst){
    var r = lst[i];
    
    csv.push(
             [
                 '"'+r.base.empno+'"'
               , '"'+r.base.empname+'"'
               , '"'+r.base.worklocation_name+'"'
               , '"'+r.view_condition.workcompany+'"'
               , '"'+r.manager.name+'"'
               , '"'+r.proctype+'"'
               , '"'+r.view_condition.retiredate+'"'
             ]
            );
  }

  return csv.map(function(p){return p.join(",")}).join("\r\n");
}
// 給与計算用CSV
function getCsvPayrollAccounting(){
  
  var json = getContractViewInfoJSON2();
  
//  for(var i in json){
//    var p = json[i];
//    Logger.log(p.view_condition.retiredate + ":p.proctype=" + p.proctype + ":p.view_condition.retiredate=" + p.view_condition.retiredate + ":" + (p.view_condition.retiredate != "" && p.proctype == "有り"));
//  }
    
//  var lst = json.filter(function($p){
//    return (
//             ($p.view_condition.retiredate != "" && $p.proctype == "有り") 
//             ||
//             ($p.proctype == "無し" || $p.proctype == "調整中（雇用形態変更）" || $p.proctype == "調整中（その他）")
//           )
//           ;
//  });  

  var lst = json;

  
  var getMaxWorkingtimesweek = function($wt){
    var arr = String($wt).split(/~|～/);
    return arr[arr.length-1];
  }
  
  var csv = [["社員番号","雇用区分","氏名","有休時間"]];
Logger.log("lst.length="+lst.length);
  for(var i in lst){
    var r = lst[i];
Logger.log("r.view_condition.workingtimes="+r.view_condition.workingtimes);
    
    var wt = getMaxWorkingtimesweek(r.view_condition.workingtimes);
    csv.push(
             [
                 '"'+r.base.empno+'"'
               , '"'+r.view_condition.contracttype+'"'
               , '"'+r.base.empname+'"'
               , '"'+wt+'"'
             ]
            );
  }

  return csv.map(function(p){return p.join(",")}).join("\r\n");
}


// 奉行更新用CSV
function getCsvUpdateBugyo($company){

  //  var tmp = 1114;
//  
//  Logger.log(Math.round(tmp * 0.25));
//  Logger.log(Math.round(tmp * 0.5));
//  Logger.log(Math.round(tmp * 0.35));
//  Logger.log(Math.round(tmp * 0.25));
//  Logger.log(Math.round(tmp * 0.6));
////  *0.25	*0.5	*0.35	*0.25	*0.6
//  
  
  var json = getContractViewInfoJSON2();
  
  var lst = json.filter(function($p){
    return $p.base.company == $company
           &&
           $p.proctype != "無し"
           &&
           $p.proctype != "更新対象外"
           &&
           (
                $p.view_condition.salarytype        != $p.old_condition.salarytype
             || $p.view_condition.salarybase        != $p.old_condition.salarybase
             || $p.view_condition.workingdays       != $p.old_condition.workingdays
             || $p.view_condition.workingtimes      != $p.old_condition.workingtimes
             || $p.view_condition.paidvacationtimes != $p.old_condition.paidvacationtimes
             
           );
  }); 
  
  // Q情報取得
  var quot = getNowQuarterValue();
  var changeDay = Moment.moment(quot.to).add("days", 1).format("YYYY/MM/DD");
Logger.log(changeDay);
  
  var csv = [['"社員番号"','"所属会社"','"雇用区分コード"','"雇用区分"','"氏名"','"職場氏名"','"給与区分コード"','"給与区分"','"基本給"','"備考"','"所定労働日数"','"労働時間"','"労働時間／週"','"有休付与日数表コード"','"有休付与日数表"','"改定年月日"','"平日所定外"','"平日所定外・深夜"','"休日勤務"','"平日所定内・深夜"','"休日深夜"','"給与区分変更"','"基本給変更"','"所定労働日数変更"','"労働時間変更"','"有休付与日数変更"']];
Logger.log("lst.length="+lst.length);
  for(var i in lst){
    var r = lst[i];
Logger.log("r.view_condition.workingtimes="+r.view_condition.workingtimes);
    
    csv.push(
             [
/*
　社員番号
　所属会社
　雇用区分コード
　雇用区分
　氏名
　職場氏名
　給与区分コード
　給与区分
　基本給
　備考
　所定労働日数
　労働時間
　労働時間／週
　有休付与日数表コード
　有休付与日数表
　改定年月日
　平日所定外
　平日所定外・深夜
　休日勤務
　平日所定内・深夜
　休日深夜

               */
                 '"'+r.base.empno+'"'
               , '"'+r.base.company+'"'
               , '"'+(r.view_condition.contracttype == "契約社員" ? "7" : "5")+'"'
               , '"'+r.view_condition.contracttype+'"'
               , '"'+r.base.empname_org+'"'
               , '"'+r.base.empname_wrk+'"'
               , '"'+(r.view_condition.salarytype == "月給" ? "0" : "2")+'"'
               , '"'+r.view_condition.salarytype+'"'
               , '"'+r.view_condition.salarybase+'"'
               , '"'+""+'"'
               , '"'+r.view_condition.workingdays+'"'
               , '"'+r.view_condition.workingtimes+'"'
               , '"'+r.view_condition.workingtimesweek+'"'
               , '"'+r.view_condition.paidvacationtimes.split(/:/)[0]+'"'
               , '"'+r.view_condition.paidvacationtimes.split(/:/)[1]+'"'
               , '"'+changeDay+'"'
               , '"'+(r.view_condition.salarytype == "月給" ? "0" : Math.round(r.view_condition.salarybase * 0.25))+'"'
               , '"'+(r.view_condition.salarytype == "月給" ? "0" : Math.round(r.view_condition.salarybase * 0.5))+'"'
               , '"'+(r.view_condition.salarytype == "月給" ? "0" : Math.round(r.view_condition.salarybase * 0.35))+'"'
               , '"'+(r.view_condition.salarytype == "月給" ? "0" : Math.round(r.view_condition.salarybase * 0.25))+'"'
               , '"'+(r.view_condition.salarytype == "月給" ? "0" : Math.round(r.view_condition.salarybase * 0.6))+'"'
               
               , '"'+(r.view_condition.salarytype   == r.old_condition.salarytype ? "" : "○")+'"'
               , '"'+(r.view_condition.salarybase   == r.old_condition.salarybase ? "" : "○")+'"'
               , '"'+(r.view_condition.workingdays  == r.old_condition.workingdays ? "" : "○")+'"'
               , '"'+(r.view_condition.workingtimes == r.old_condition.workingtimes ? "" : "○")+'"'
               , '"'+(r.view_condition.paidvacationtimes == r.old_condition.paidvacationtimes ? "" : "○")+'"'

             ]
            );
  }
 
  
  return csv.map(function(p){return p.join(",")}).join("\r\n");
}

// 社用アドレス一覧CSV
function getCsvMailaddress(){
  
  var json = getContractViewInfoJSON2();
  var lst = json;
  var csv = [["社員番号","雇用区分","氏名","所属部署","更新有無ステータス","社用e-mail"]];
Logger.log("lst.length="+lst.length);
  for(var i in lst){
    var r = lst[i];
    
    csv.push(
             [
                 '"'+r.base.empno+'"'
               , '"'+r.view_condition.contracttype+'"'
               , '"'+r.base.empname+'"'
               , '"'+r.base.affiliation_name+'"'
               , '"'+r.proctype+'"'
               , '"'+r.base.mail+'"'
             ]
            );
  }

  return csv.map(function(p){return p.join(",")}).join("\r\n");
}


/*
* ログインユーザが管理する従業員情報を取得
*/
function getContractViewInfoJSON($mail,$all){
 Logger.log($mail);
 
  var json = getContractViewInfoJSON2();
  var lst = json.filter(function($p){
    return $p.manager.id == $mail || $p.managersub.id == $mail || $all;
  });
  
Logger.log("lst="+lst.length);

  return lst;
  
}

/**
* 勤続年数文字情報取得処理(#年#ヶ月)
* @param  {string} d 日付文字列
*/
function getWorkhist(d){
  var n = Moment.moment();
  var diffMonths = n.diff(d, 'months'); 
  var diffYears = Math.floor(diffMonths / 12);
  var ret = (diffYears > 0 ? diffYears+"年" : "")+
            ((diffMonths - (diffYears * 12)) == 0 ? "" : (diffMonths - (diffYears * 12))+"ヶ月");
  return ret;
}

/**
* 契約従業員情報取得
*/
function getContractViewInfoJSON2(){
  // 1.対象情報一覧  
  var values = getSpreadsheetValues("契約書", 3, 1, 62);
  // 2.組織図登録従業員情報検索用関数
  var empSearch = getSearchEmpFunction();
  // 3.情報設定
  var arr=[];
  for(var i = 0;i<values.length;i++){
    
      // 管理者付属情報付与      
      var managers = values[i][0].split(",");
      var manager    = {"id":"", "name":"", "affiliation":"", "affiliation_name":""}
      var managersub = {"id":"", "name":"", "affiliation":"", "affiliation_name":""}
      if(managers.length > 0){
        var inf = empSearch(managers[0]);
        if(inf){
          manager = {"id":inf.mail, "name":inf.name, "affiliation":inf.affiliation, "affiliation_name":inf.affiliation_name}
        }
      }
      if(managers.length > 1){
        var inf = empSearch(managers[1]);
        if(inf){
          managersub = {"id":inf.mail, "name":inf.name, "affiliation":inf.affiliation, "affiliation_name":inf.affiliation_name}
        }
      }
                     
      var empinf = empSearch(values[i][1]);
      
      // 勤続年数
      var workhist = getWorkhist(Moment.moment(values[i][10]));
      var json_tmp = {
                          "rowid"         : (i + 3)
                        , "manager"       : manager
                        , "managersub"    : managersub
                        , "base"          : {
                                             "empno"    : values[i][1]
                                           , "empname"  : values[i][5] + (values[i][5] != values[i][6] ? "(" + values[i][6] + ")" : "")
                                           , "empname_org" : values[i][5]
                                           , "empname_wrk" : values[i][6]
                                           , "hiredate" : Moment.moment(values[i][10]).format("YYYY/MM/DD")
                                           , "workhist" : workhist
                                           , "affiliation"           :  empinf ? empinf.affiliation : ""
                                           , "affiliation_name"      :  empinf ? empinf.affiliation_name : ""
                                           , "worklocation"          :  empinf ? empinf.worklocation : ""
                                           , "worklocation_name"     :  empinf ? empinf.worklocation_name : ""
                                           , "leavestate"            :  empinf ? (empinf.leavestate == "1" ? "休職" : "在籍") : "退職"
                                           , "company"               :  values[i][11]
                                           , "mail"                  :  empinf ? empinf.mail : ""
                          
                                           
                                           , "empins"                : values[i][7]
                                           , "socins"                : values[i][8]
                                           , "socins_loss"           : values[i][9]
                                           , "last_salary_raise"     : values[i][17]
                                           , "last_salary_raisememo" : values[i][18]
                                           
                                            }
                                            
                        , "proctype"      : values[i][35]
                        
                        , "old_condition" : {
                                             "workcompany"       : values[i][2]
                                           , "job"               : values[i][3]
                                           , "contracttype"      : values[i][4]
                                           , "contractfixdate"   : values[i][14] == "" ? "" : Moment.moment(values[i][14]).format("YYYY/MM/DD")
                                           , "retiredate"        : ""
                                           , "salarytype"        : values[i][15]
                                           , "salarybase"        : Number(values[i][16])
                                           , "payrisememo"       : ""
                                           
                                           , "workingdays"       : values[i][19]
                                           , "workingtimes"      : values[i][20]
                                           , "workingtimesweek"  : values[i][21]
                                           , "paidvacationtimes" : values[i][22]
                                           
                                           , "hourcondition1"    : {
                                                                       "from"   : values[i][23]
                                                                     , "to"     : values[i][24]
                                                                     , "week"   : values[i][25]
                                                                     , "memo"   : values[i][26]
                                                                   }

                                           , "hourcondition2"    : {
                                                                       "from"   : values[i][27]
                                                                     , "to"     : values[i][28]
                                                                     , "week"   : values[i][29]
                                                                     , "memo"   : values[i][30]
                                                                   }
                                           
                                           , "hourcondition3"    : {
                                                                       "from"   : values[i][31]
                                                                     , "to"     : values[i][32]
                                                                     , "week"   : values[i][33]
                                                                     , "memo"   : values[i][34]
                                                                   }
                                           , "laborinterview"    : ""
                                           , "laborinterviewmemo": ""
                                       }
                        , "view_condition" : {
                                             "workcompany"       : getCP(values[i][2],  values[i][36])
                                           , "job"               : getCP(values[i][3],  values[i][37])
                                           , "contracttype"      : getCP(values[i][4],  values[i][38])
                                           , "contractfixdate"   : getCP(values[i][14], values[i][39]) == "" ? "" : Moment.moment(getCP(values[i][14], values[i][39])).format("YYYY/MM/DD")
                                           , "retiredate"        : values[i][40] == "" ? "" : Moment.moment(values[i][40]).format("YYYY/MM/DD")
                                           , "salarytype"        : getCP(values[i][15], values[i][41])
                                           , "salarybase"        : Number(getCP(values[i][16], values[i][42]))
                                           , "payrisememo"       : values[i][43]
                                           
                                           , "workingdays"       : getCP(values[i][19], values[i][44])
                                           , "workingtimes"      : getCP(values[i][20], values[i][45])
                                           , "workingtimesweek"  : getCP(values[i][21], values[i][46])
                                           , "paidvacationtimes" : getCP(values[i][22], values[i][47])
                                           
                                           , "hourcondition1"    : {
                                                                       "from"   : getCP(values[i][23], values[i][48])
                                                                     , "to"     : getCP(values[i][24], values[i][49])
                                                                     , "week"   : getCP(values[i][25], values[i][50])
                                                                     , "memo"   : getCP(values[i][26], values[i][51])
                                                                   }

                                           , "hourcondition2"    : {
                                                                       "from"   : getCP(values[i][27], values[i][52])
                                                                     , "to"     : getCP(values[i][28], values[i][53])
                                                                     , "week"   : getCP(values[i][29], values[i][54])
                                                                     , "memo"   : getCP(values[i][30], values[i][55])
                                                                   }
                                           
                                           , "hourcondition3"    : {
                                                                       "from"   : getCP(values[i][31], values[i][56])
                                                                     , "to"     : getCP(values[i][32], values[i][57])
                                                                     , "week"   : getCP(values[i][33], values[i][58])
                                                                     , "memo"   : getCP(values[i][34], values[i][59])
                                                                   }
                                           , "laborinterview"    : values[i][60]
                                           , "laborinterviewmemo": values[i][61]
                                       }
                      };
        arr.push(json_tmp);

//    }
    
  }
Logger.log("--[END]getContractViewInfoJSON--");
  
  return arr;

}



/**
* 配列分割
* @param  []  targetArray 対象配列
* @param  int splitCount 分割指定数
*/
function arraySplit(targetArray, splitCount){
 
  var baseCnt = targetArray.length,  // 対象件数 
      cnt    = splitCount,           // 分割指定数
      newArr = [];                   // 戻値用配列

  for(var i = 0; i < Math.ceil(baseCnt / cnt); i++) {
    var j = i * cnt;
    var p = targetArray.slice(j, j + cnt);
    newArr.push(p);
  }  
  
  return newArr;
}


/**
* 周知メール送信処理
* @param  {template,cc,bcc} mailinfo メール雛形
* ※現在はメール送信せずアドレス取得
*/
function sendDisseminateMail(mailinfo){
Logger.log("--sendDisseminateMail--");
Logger.log(mailinfo.template);
Logger.log(mailinfo.cc);
Logger.log(mailinfo.bcc);
  
  // 1.契約情報取得
  var json = getContractViewInfoJSON2();
  
  // 2.送付アドレス取得
  var toaddress = [];
  for(var i in json){
    var emp = json[i];
    
    if(emp.manager.id == "" && emp.managersub.id == ""){
      throw "担当者が設定されていない契約があります。全ての担当者を設定後に対応ください。";
    }
    else
    {
      var mails = [emp.manager.id, emp.managersub.id];
      for(var j in mails){
        if(mails[j] != "" && toaddress.indexOf(mails[j]) == -1){
         toaddress.push(mails[j]);
        }
      }
    }
  }


  
//  // メール送信
//  Logger.log("toaddress.length="+toaddress.length);
//  var splitToaddress = arraySplit(toaddress,30);
//  Logger.log(splitToaddress);
//  
//  
////        throw "強制エラーでメール飛ばしません。";
//
//  for(var i in splitToaddress){
//
//Logger.log(splitToaddress[i]);    
//    
//    GmailApp.sendEmail(
//      splitToaddress[i].join(","), 
//      "【重要】非正規従業員の契約更新情報を入力してください", 
//      mailinfo.template, 
//      {
//        from: "roumu_account@leverages.jp", 
//        name: "レバレジーズ　労務担当",
//        cc:mailinfo.cc,
//        bcc:mailinfo.bcc
//      }
//    );
//    
//    
//  }
  
  return toaddress;
  
  
}


/**
* リマインドメール送信処理
* @param  {string} mailinfo メール雛形
* ※現在はメール送信せずアドレス取得
*/
function sendRemindMail(mailinfo){
  // 1.契約情報取得
  var json = getContractViewInfoJSON2();

  // 2.対象ステータスピックアップ
  var target = json.filter(function($p){
    return $p.proctype == "" || $p.proctype == "調整中（雇用形態変更）" || $p.proctype == "調整中（その他）";
  });

  // 3.対象有無確認
  if(target.length == 0){
    throw "催促対象の契約が存在しません。";
  }
  
  // 4.送付アドレス取得
  var toaddress = [];
  for(var i in target){
    var emp = target[i];

    if(emp.manager.id == "" && emp.managersub.id == ""){
      throw "担当者が設定されていない契約があります。全ての担当者を設定後に対応ください。";
    }
    else
    {
      var mails = [emp.manager.id, emp.managersub.id];
      for(var j in mails){
        if(mails[j] != "" && toaddress.indexOf(mails[j]) == -1){
         toaddress.push(mails[j]);
        }
      }
    }
  }
  
  // 3.メール送付
//    GmailApp.sendEmail(
//    toaddress.join(","), 
//    "【リマインド：重要】非正規従業員の契約更新情報を入力してください", 
//    mailinfo.template, 
//    {
//      from: "roumu_account@leverages.jp", 
//      name: "レバレジーズ　労務担当",
//      cc:mailinfo.cc,
//      bcc:mailinfo.bcc    
//    }
//    );  

  // 4.対象メールアドレス返却
  return toaddress;
  
}
///*
//  イメージファイルID取得
//*/
//function getImageFileId(nm){
//  var fd = DriveApp.getFoldersByName("ContractManage").next().getFoldersByName("web").next();
//  if(fd.getFilesByName(nm).hasNext()){
//    var f  = fd.getFilesByName(nm).next();
//    return f.getId();
//  } else {
//    return "";
//  }
//}

/**
* 奉行情報取得処理
* @param  {string} type 種別
* @return {string} 指定した種別のJSON情報を返す
*/
function getJsonMst(type){
  
  var app = SpreadsheetApp.getActiveSpreadsheet();
  var st = app.getSheetByName("mstjson");
  var lastrow = st.getLastRow();
  var rg = st.getRange(2, 1, lastrow, 21);
  var values = rg.getValues();
  var returnValue = "";
  for(var i = 0;i<values.length;i++){
    var v = values[i];
    if(v[0] == type){
      for(var idx = 1;idx<=20;idx++){
        returnValue+=v[idx];
      }
    }
  }
  
  return JSON.parse(returnValue);
}

/**
* 従業員検索用関数
* @return function 従業員IDをkeyに氏名情報を返す関数
*/
function getSearchEmpFunction(){

  //{"type":"勤務地","id":"001","name":"東京本社"}
  var dicWorkplace = [];
  var allTypes = getJsonMst("区分");
  var searchTypes = function($type, $id){
    var retT = allTypes.filter(function(p){
      return p.type == $type && p.id == $id;
    });
    return retT.length == 0 ? "" : retT[0].name;
  };  
  
  var allEmployee = getJsonMst("従業員");
  var searchEmp = function($idOrMail){
    var ret = allEmployee.filter(function(p){
      return p.id == $idOrMail || p.mail == $idOrMail;
    });
    // 区分名称付与
    var result = null;
    if(ret.length > 0){
      result = ret[0];
      result.worklocation_name = searchTypes("勤務地",result.worklocation);
    }
    return result;
  };
   
  return searchEmp;
}